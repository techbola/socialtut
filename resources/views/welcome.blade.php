@extends('layouts.master')

@section('title')
    Welcome!
@endsection

@section('content')

    @include('includes.message')

    <div class="row">

        <div class="col-md-4 col-md-offset-4">
            <h3>Create Account</h3>
            <form action="{{ route('signup') }}" method="post">

                {{--{{ csrf_field() }}--}}

                <input type="hidden" name="_token" value="{{ Session::token() }}">

                <div class="form-group">
                    <label for="email">E-Mail</label>
                    <input type="email" name="email" id="email" value="{{ Request::old('email') }}" class="form-control {{ $errors->has('email') ? 'border-danger' : '' }}" required>
                </div>

                <div class="form-group">
                    <label for="first_name">First Name</label>
                    <input type="text" name="first_name" id="first_name" value="{{ Request::old('first_name') }}" class="form-control {{ $errors->has('first_name') ? 'border-danger' : '' }}" required>
                </div>

                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" id="password" class="form-control {{ $errors->has('password') ? 'border-danger' : '' }}" required>
                </div>

                <button type="submit" class="btn btn-primary">Register</button>
            </form>
        </div>

        <div class="col-md-6">
            <h3>Sign In</h3>
            <form action="{{ route('signin') }}" method="post">

                <input type="hidden" name="_token" value="{{ Session::token() }}">

                <div class="form-group">
                    <label for="email">E-Mail</label>
                    <input type="email" name="email" id="email" value="{{ Request::old('email') }}" class="form-control {{ $errors->has('email') ? 'border-danger' : '' }}" required>
                </div>

                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" id="password" class="form-control {{ $errors->has('password') ? 'border-danger' : '' }}" required>
                </div>

                <button type="submit" class="btn btn-primary">Login</button>
            </form>
        </div>

    </div>
@endsection