<?php

Route::get('/', function () {
    return view('welcome');
})->name('home');

Route::post('/signup', [
    'uses' => 'UserController@postSignUp',
    'as' => 'signup'
]);

Route::post('/signin', [
    'uses' => 'UserController@postSignIn',
        'as' => 'signin'
]);

Route::post('/createpost', [
    'uses' => 'PostController@CreatePost',
    'as' => 'post.create'
]);

Route::get('/logout', [
    'uses' => 'UserController@getLogout',
    'as' => 'logout'
]);

Route::get('/account', [
    'uses' => 'UserController@getAccount',
    'as' => 'account'
]);

Route::post('/updateaccount', [
    'uses' => 'UserController@UpdateAccount',
    'as' => 'account.save'
]);

Route::get ('/userimage/{filename}', [
    'uses' => 'UserController@getUserImage',
    'as' => 'account.image'
]);

Route::group(['middleware' => 'auth'], function() {

    Route::get('/dashboard', [
        'uses' => 'PostController@getDashboard',
        'as' => 'dashboard'
    ]);

    Route::get('/delete/post/{post_id}', [
        'uses' => 'PostController@getDeletePost',
        'as' => 'post.delete'
    ]);

//    Route::post('/edit', function(\Illuminate\Http\Request $request){
//        //returns json response
//        return response()->json(['message' => $request['postId']]);
//    })->name('edit');

    Route::post('/edit', [
        'uses' => 'PostController@EditPost',
        'as' => 'edit'
    ]);

    Route::post('/like', [
        'uses' => 'PostController@LikePost',
        'as' => 'like'
    ]);

});

