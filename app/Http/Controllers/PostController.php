<?php

namespace App\Http\Controllers;

use App\Post;
use App\Like;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    public function getDashboard()
    {
        $posts = Post::orderBy('created_at', 'desc')->get();
        return view('dashboard', [
            'posts' => $posts
        ]);
    }

    public function CreatePost(Request $request)
    {
        $this->validate($request, [
            'body' => 'required|max:1000'
        ]);
        $post = new Post();
        $post->body = $request['body'];
        if($request->user()->posts()->save($post)){
            $message = 'Post Successfully Created';
        }

        return redirect()->route('dashboard')->with([
            'message' => $message
        ]);
    }

    public function getDeletePost($post_id)
    {
        //$post = Post::find($id);

        $post = Post::where('id', $post_id)->first();

        if(Auth::user() != $post->user){
            return redirect()->back();
        }

        $post->delete();

        return redirect()->route('dashboard')->with([
            'message' => 'Post Deleted'
        ]);
    }

    public function EditPost(Request $request)
    {
        $this->validate($request, [
            'body' => 'required|max:1000'
        ]);
        $post = Post::find($request['postId']);

        if(Auth::user() != $post->user){
            return redirect()->back();
        }

        $post->body = $request['body'];
        $post->update();

        //return response()->json(['message' => 'Post Edited'], 200);

        //    change the post body immediately after clicking save changes
        return response()->json(['new_body' => $post->body], 200);

    }

    public function LikePost(Request $request)
    {
        $postId = $request['postId'];
        $isLike = $request['isLike'] == 'true';

        $update = false;

        $post = Post::find($postId);

        if(!$post){
            return null;
        }

        $user = Auth::user();

        $like = $user->likes()->where('post_id', $postId)->first();

        if($like){
            $alreay_like = $like->like;
            $update = true;

            if($alreay_like == $isLike){
                $like->delete();
                return null;
            }
        } else{
            $like = new Like();
        }

        $like->like = $isLike;
        $like->user_id = $user->id;
        $like->post_id = $post->id;

        if($update){
            $like->update();
        } else{
            $like->save();
        }
        return null;
    }

}
