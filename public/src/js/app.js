var postId = 0;
var postBodyElement = null;

$('.edit-button').click(function(e) {
    e.preventDefault();
   // console.log('It Works!');

    postBodyElement = e.target.parentNode.parentNode.childNodes[1];
    var postBody = postBodyElement.textContent;
    postId = e.target.parentNode.parentNode.dataset['postid'];
    //console.log(postBody);
    $('#post-body').val(postBody);
    $('#edit-modal').modal();

});

$('#modal-save').on('click', function(){
    $.ajax({
        method: 'POST',
        url: urlEdit,
        data: { body: $('#post-body').val(), postId: postId, _token: token}
    })
    .done(function (msg){
        //console.log(msg['message']);

        //show the result in json format
        //console.log(JSON.stringify(msg));

    //    change the post body immediately after clicking save changes
        $(postBodyElement).text(msg['new_body']);
        $('#edit-modal').modal('hide');

    });
});

$('.like').on('click', function(e){
    e.preventDefault();
    postId = e.target.parentNode.parentNode.dataset['postid'];
    var isLike = e.target.previousElementSibling == null;
    $.ajax({
        method: 'POST',
        url: urlLike,
        data: {isLike: isLike, postId: postId, token: token}
    })
        .done(function() {
            e.target.innerText = isLike ? e.target.innerText == 'Like' ?
                'You like this post' : 'Like' : e.target.innerText == 'Dislike'
                ? 'You do not like this post' : 'Dislike';

            if (isLike){
                e.target.nextElementSibling.innerText = 'Dislike';
            }
            else{
                e.target.previousElementSibling.innerText = 'Like';
            }
    });
});